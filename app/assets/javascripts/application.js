// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap_sb_admin_base_v2
//= require_tree .

function selectAllDays() {
    var all = document.getElementById("Todos").checked;
    var l = document.getElementById("Lunes");
    if (l != null) {
        document.getElementById(l.id).checked = all;
        updateJson(l.id);
    }
    var ma = document.getElementById("Martes");
    if (ma != null) {
        document.getElementById(ma.id).checked = all;
        updateJson(ma.id);
    }
    var mi = document.getElementById("Miércoles");
    if (mi != null) {
        document.getElementById(mi.id).checked = all;
        updateJson(mi.id);
    }
    var j = document.getElementById("Jueves");
    if (j != null) {
        document.getElementById(j.id).checked = all;
        updateJson(j.id);
    }
    var v = document.getElementById("Viernes");
    if (v != null) {
        document.getElementById(v.id).checked = all;
        updateJson(v.id);
    }
    var s = document.getElementById("Sábado");
    if (s != null) {
        document.getElementById(s.id).checked = all;
        updateJson(s.id);
    }
    var d = document.getElementById("Domingo");
    if (d != null) {
        document.getElementById(d.id).checked = all;
        updateJson(d.id);
    }
}

function selectDay(day) {
    var checked = document.getElementById(day).checked;
    document.getElementById(day + '_input').hidden = !checked;
}

function changePeriods() {
    var periods = document.getElementById("periods").value;
    var duration1 = document.getElementById("1_duration");
    var duration2 = document.getElementById("2_duration");
    var duration3 = document.getElementById("3_duration");
    var duration4 = document.getElementById("4_duration");
    duration1.hidden = true;
    duration2.hidden = true;
    duration3.hidden = true;
    duration4.hidden = true;
    if (periods == 1) {
        duration1.hidden = false;
    } else if (periods == 2) {
        duration1.hidden = false;
        duration2.hidden = false;
    } else if (periods == 3) {
        duration1.hidden = false;
        duration2.hidden = false;
        duration3.hidden = false;
    } else if (periods == 4) {
        duration1.hidden = false;
        duration2.hidden = false;
        duration3.hidden = false;
        duration4.hidden = false;
    }
}
function fix() {
    var search = document.getElementById("dataTables-example_filter");
    if (search != null) search.hidden = true;
    var show = document.getElementById("dataTables-example_length");
    if (show != null) {
        var label = show.children[0];
        label.innerHTML = label.innerHTML.replace("Show", "Mostrar").replace(" entries", "");
    }
    var showing = document.getElementById("dataTables-example_info");
    if (showing != null) showing.innerHTML = showing.innerHTML.replace("Showing", "Mostrando").replace("to", "-").replace("of", "de").replace(" entries", "");
    var previous = document.getElementById("dataTables-example_previous");
    if (previous != null) previous.innerHTML = previous.innerHTML.replace("Previous", "<");
    var next = document.getElementById("dataTables-example_next");
    if (next != null) next.innerHTML = next.innerHTML.replace("Next", ">");
}