json.array!(@commissions) do |commission|
  json.extract! commission, :id, :n, :subject_id
  json.url commission_url(commission, format: :json)
end
