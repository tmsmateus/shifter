json.array!(@lessons) do |lesson|
  json.extract! lesson, :id, :name, :room_id, :period_id
  json.url lesson_url(lesson, format: :json)
end
