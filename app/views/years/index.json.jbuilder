json.array!(@years) do |year|
  json.extract! year, :id, :career_id, :n
  json.url year_url(year, format: :json)
end
