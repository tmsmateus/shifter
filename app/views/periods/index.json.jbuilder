json.array!(@periods) do |period|
  json.extract! period, :id, :n, :hours, :minutes, :commission_id
  json.url period_url(period, format: :json)
end
