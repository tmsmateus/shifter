json.array!(@shifts) do |shift|
  json.extract! shift, :id, :initial_hour, :initial_minute, :final_hour, :final_minute
  json.url shift_url(shift, format: :json)
end
