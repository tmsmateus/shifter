json.array!(@availabilities) do |availability|
  json.extract! availability, :id, :initial_hour, :initial_minute, :final_hour, :final_minute
  json.url availability_url(availability, format: :json)
end
