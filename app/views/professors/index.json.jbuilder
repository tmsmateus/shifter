json.array!(@professors) do |professor|
  json.extract! professor, :id, :name, :surname, :n
  json.url professor_url(professor, format: :json)
end
