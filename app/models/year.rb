class Year < ActiveRecord::Base
  belongs_to :career
  has_and_belongs_to_many :shifts

  def add_shift(shift)
    shifts << shift
    save
  end
end
