class Professor < ActiveRecord::Base
  has_and_belongs_to_many :shifts
  has_and_belongs_to_many :periods
  has_and_belongs_to_many :availabilities

  def add_availability(availability)
    availabilities << availability
    save
  end

  def add_shift(shift)
    shifts << shift
    save
  end

  def add_period(period)
    periods << period unless periods.include? period
    save
  end

  def clear_shifts
    shifts.each do |shift|
      shifts.delete shift
      shift.destroy
    end
    save
  end

  def clear_availabilities
    availabilities.each do |availability|
      availabilities.delete availability
      availability.destroy
    end
    save
  end

  def clear_periods
    periods.each do |period|
      periods.delete period
    end
    save
  end

  def update_availabilities(shift)
    shift_day = shift.day
    shift_h_i = shift.initial_hour
    shift_m_i = shift.initial_minute
    shift_h_f = shift.final_hour
    shift_m_f = shift.final_minute
    t_i = (shift_h_i * 100) + shift_m_i
    t_f = (shift_h_f * 100) + shift_m_f

    self.availabilities.each do |availability|
      av_day = availability.day
      av_h_i = availability.initial_hour
      av_m_i = availability.initial_minute
      av_h_f = availability.final_hour
      av_m_f = availability.final_minute
      if av_day == shift_day
        i = t_i - (av_h_i * 100) - av_m_i
        f = t_f - (av_h_f * 100) - av_m_f
        if i > 0
          av = Availability.create({ initial_hour: av_h_i, initial_minute: av_m_i,
                                     final_hour: shift_h_i, final_minute: shift_m_i })
          av.set_day(av_day)
          av.save
          add_availability(av)
        end
        if f < 0
          av = Availability.create({ initial_hour: shift_h_f, initial_minute: shift_m_f,
                                     final_hour: av_h_f, final_minute: av_m_f })
          av.set_day(av_day)
          av.save
          add_availability(av)
        end
        availability.delete
      end
    end
  end

  def full_name
    name + ' ' + surname
  end
end