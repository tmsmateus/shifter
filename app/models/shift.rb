class Shift < ActiveRecord::Base
  has_and_belongs_to_many :days
  has_and_belongs_to_many :lessons
  has_and_belongs_to_many :professors
  has_and_belongs_to_many :rooms
  has_and_belongs_to_many :years

  def day
    days[0]
  end

  def time
    m_i = initial_minute == 0 ? '00' : initial_minute.to_s
    m_f = final_minute == 0 ? '00' : final_minute.to_s
    initial_hour.to_s + ':' + m_i + ' a ' + final_hour.to_s + ':' + m_f
  end

  def set_day(day)
    days << day
    save
  end

  def daytime
    day.name + ': ' + initial_hour.to_s + ':' + initial_minute.to_s + ' - ' + final_hour.to_s + ':' + final_minute.to_s
  end
end
