class Availability < ActiveRecord::Base
  has_and_belongs_to_many :days
  has_and_belongs_to_many :professors
  has_and_belongs_to_many :rooms

  def day
    days[0]
  end

  def set_day(day)
    days << day
    save
  end

  def time
    day.name + ': ' + initial_hour.to_s + ':' + initial_minute.to_s + ' - ' + final_hour.to_s + ':' + final_minute.to_s
  end
end
