class Career < ActiveRecord::Base
  has_and_belongs_to_many :subjects

  def years
    Year.where(career_id: self.id)
  end

  def clear_shifts
    years.each do |year|
      year.shifts.each do |shift|
        year.shifts.delete shift
        shift.destroy
      end
    end
    save
  end

  def clear_years
    years.each do |year|
      years.delete year
      year.destroy
    end
    save
  end

  def add_year(year)
    years << year
    save
  end

  def add_subject(subject)
    subject.careers.each do |career|
      subject.careers.delete career
    end
    subjects << subject unless subjects.include?(subject)
    save
  end
end
