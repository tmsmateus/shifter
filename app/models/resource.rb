class Resource < ActiveRecord::Base
  has_and_belongs_to_many :rooms
  has_and_belongs_to_many :periods
end
