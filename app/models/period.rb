class Period < ActiveRecord::Base
  belongs_to :commission
  has_and_belongs_to_many :professors
  has_and_belongs_to_many :resources

  def subject
    commission.subject
  end

  def lessons
    Lesson.where(period_id: self.id)
  end

  def add_resource(resource)
    resources << resource
    save
  end

  def clear_professors
    professors.each do |professor|
      professor.periods.delete self
      professor.save
    end
    save
  end

  def clear_resources
    resources.each do |resource|
      resources.delete resource
    end
    save
  end
end
