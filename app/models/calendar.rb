class Calendar < ActiveRecord::Base
  def lessons
    Lesson.where(calendar_id: self.id)
  end

  def career_id
    lessons[0].subject.career.id
  end

  def year
    lessons[0].subject.year
  end

  def quarter
    lessons[0].subject.quarter
  end
end
