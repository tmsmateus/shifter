class Lesson < ActiveRecord::Base
  has_one :room
  has_one :period
  has_one :shift
  belongs_to :calendar
  # has_and_belongs_to_many :shifts

  def room
    Room.find(room_id)
  end

  def period
    Period.find(period_id)
  end

  def shift
    Shift.find(shift_id)
  end

  def subject
    return nil if period.nil?
    return nil if period.commission.nil?
    period.commission.subject
  end
end
