class Day < ActiveRecord::Base
  has_and_belongs_to_many :availabilities
  has_and_belongs_to_many :shifts
end
