class Subject < ActiveRecord::Base
  has_and_belongs_to_many :careers

  def career
    careers[0]
  end

  def commissions
    Commission.where(subject_id: self.id)
  end

  def periods
    commissions[0].periods
  end

  def clear_periods
    commissions.each do |commission|
      commission.periods.each do |period|
        commission.periods.delete period
        period.destroy
      end
    end
    save
  end

  def clear_commissions
    commissions.each do |commission|
      commissions.delete commission
      commission.destroy
    end
    save
  end

  def clear_resources
    resources.each do |resource|
      resources.delete resource
    end
    save
  end
end
