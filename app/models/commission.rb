class Commission < ActiveRecord::Base
  belongs_to :subject

  def periods
    Period.where(commission_id: self.id)
  end
end
