# CUANDO HAY UNA RELACION 1 A N, SI SE CAMBIA EL 1, HAY QUE HACE LO QUE HACE Career.add_subject
# VALIDAR QUE NO SE PUEDA ELIMINAR 1 SI EXISTEN Ns
class CareersController < ApplicationController
  before_action :set_career, only: [:show, :edit, :update, :destroy, :custom]
  before_action :validate_session

  # GET /careers
  # GET /careers.json
  def index
    @careers = Career.all
    id = params[:id]
    json = params[:json]
    unless id.nil? || json.nil?
      career = Career.find(id)
      unless validate_time(json)
        @career = career
        @years = @career.years
        @error = true
        respond_to do |format|
          format.html { render :custom }
        end
      else
        career.clear_shifts
        json.split(',').each do |a|
          b = a.split(':')
          year = Year.where(career_id: id, n: b[0])[0]
          time = b[1]
          $week.each do |day|
            shift = Shift.create(initial_hour: time[0,2].to_i, initial_minute: time[2,2].to_i, final_hour: time[4,2].to_i, final_minute: time[6,2].to_i)
            shift.set_day(day)
            year.add_shift(shift)
          end
        end
        career.save
        redirect_to careers_url, notice: 'Los horarios se guardaron correctamente'
      end
    end
  end

  # GET /careers/1
  # GET /careers/1.json
  def show
  end

  # GET /careers/new
  def new
    @career = Career.new
  end

  # GET /careers/1/edit
  def edit
  end

  # POST /careers
  # POST /careers.json
  def create
    unless validate_career
      @career = Career.new
      @error = true
      respond_to do |format|
        format.html { render :new }
      end
    else
      @career = Career.new(career_params)
      create_career(@career)
      respond_to do |format|
        if @career.save
          format.html { redirect_to careers_url, notice: 'La carrera se guardó correctamente' }
          format.json { render :show, status: :created, location: @career }
        else
          format.html { render :new }
          format.json { render json: @career.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /careers/1
  # PATCH/PUT /careers/1.json
  def update
    unless validate_career
      @error = true
      respond_to do |format|
        format.html { render :edit, career_id: @career }
      end
    else
      unless @career.subjects.empty?
        respond_to do |format|
          format.html { redirect_to edit_career_path(@career), alert: 'No se pudo editar la carrera porque contiene materias' }
        end
      else
        clear_career(@career)
        create_career(@career)
        respond_to do |format|
          if @career.update(career_params)
            format.html { redirect_to careers_url, notice: 'La carrera se guardó correctamente' }
            format.json { render :show, status: :ok, location: @career }
          else
            format.html { render :edit }
            format.json { render json: @career.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  # DELETE /careers/1
  # DELETE /careers/1.json
  def destroy
    unless @career.subjects.size == 0
      respond_to do |format|
        format.html { redirect_to careers_url, alert: 'No se pudo eliminar la carrera porque contiene materias' }
      end
    else
      clear_career(@career)
      @career.destroy
      respond_to do |format|
        format.html { redirect_to careers_url, notice: 'La carrera se eliminó correctamente' }
        format.json { head :no_content }
      end
    end
  end

  # GET /careers/1/custom
  def custom
    @years = @career.years;
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_career
      @career = Career.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def career_params
      params.require(:career).permit(:name)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end

  def create_career(career)
    years = params[:years].to_i
    i = 0
    while i < years do
      i = i + 1
      year = Year.create(career: career, n: i)
      $week.each do |day|
        shift = Shift.create(initial_hour: 7, initial_minute: 0, final_hour: 23, final_minute: 0)
        shift.set_day(day)
        year.add_shift(shift)
      end
      year.save
      career.add_year(year)
    end
  end

  def clear_career(career)
    career.clear_shifts
    career.clear_years
  end

  def validate_career
    v_1 = !params[:career][:name].empty?
    years = params[:years]
    v_2 = !years.empty? && years.to_i > 0 && years.to_i < 10
    v_1 && v_2
  end

  def validate_time(json)
    json.split(',').each do |a|
      time = a.split(':')[1]
      return false if time[0,4].to_i >= time[4,4].to_i
    end
    true
  end
end
