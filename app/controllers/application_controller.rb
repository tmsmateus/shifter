class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  $week = Day.all[0..4]
  $quarters = 2

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def to_number(h, m)
    (h * 1000) + m
  end

  def to_time(n)
    h = (n / 1000).to_i
    m = n - h * 1000
    [h, m]
  end

  def intersection(shift_1, shift_2)
    t_i_1 = to_number(shift_1.initial_hour, shift_1.initial_minute)
    t_f_1 = to_number(shift_1.final_hour, shift_1.final_minute)
    t_i_2 = to_number(shift_2.initial_hour, shift_2.initial_minute)
    t_f_2 = to_number(shift_2.final_hour, shift_2.final_minute)
    return nil if t_f_1 < t_i_2 || t_f_2 < t_i_1 || shift_1.day.id != shift_2.day.id
    t_i = to_time(t_i_1 > t_i_2 ? t_i_1 : t_i_2)
    t_f = to_time(t_f_1 < t_f_2 ? t_f_1 : t_f_2)
    return nil if t_i == t_f
    shift = Shift.new
    shift.initial_hour = t_i[0]
    shift.initial_minute = t_i[1]
    shift.final_hour = t_f[0]
    shift.final_minute = t_f[1]
    shift.days << shift_1.day
    shift
  end

  def difference(shift_1, shift_2)
    intersection = intersection(shift_1, shift_2)
    return [shift_1] if intersection.nil?
    shifts = Array.new
    t_i_1 = to_number(shift_1.initial_hour, shift_1.initial_minute)
    t_f_1 = to_number(shift_1.final_hour, shift_1.final_minute)
    t_i_2 = to_number(shift_2.initial_hour, shift_2.initial_minute)
    t_f_2 = to_number(shift_2.final_hour, shift_2.final_minute)
    if t_i_1 < t_i_2
      t_i = to_time(t_i_1)
      t_f = to_time(t_i_2)
      shift = Shift.new
      shift.initial_hour = t_i[0]
      shift.initial_minute = t_i[1]
      shift.final_hour = t_f[0]
      shift.final_minute = t_f[1]
      shift.days << shift_1.day
      shifts << shift
    end
    if t_f_2 < t_f_1
      t_i = to_time(t_f_2)
      t_f = to_time(t_f_1)
      shift = Shift.new
      shift.initial_hour = t_i[0]
      shift.initial_minute = t_i[1]
      shift.final_hour = t_f[0]
      shift.final_minute = t_f[1]
      shift.days << shift_1.day
      shifts << shift
    end
    shifts
  end

  def intersections(shifts_1, shifts_2)
    shifts = Array.new
    shifts_1.each do |shift_1|
      shifts_2.each do |shift_2|
        shift = intersection(shift_1, shift_2)
        shifts << shift unless shift.nil?
      end
    end
    shifts
  end

  def differences(shifts_1, shifts_2)
    return shifts_1 if shifts_2.empty?
    shifts = Array.new
    shifts_1.each do |shift_1|
      ss = Array.new
      # s = nil
      shifts_2.each do |shift_2|
        differences = difference(shift_1, shift_2)
        ss = ss.empty? ? differences : intersections(ss, differences)
      end
      ss.each do |s|
        shifts << s unless s.nil?
      end
    end
    shifts
  end

  def get_shifts(lessons)
    shifts = Array.new
    lessons.each do |lesson|
      shifts << lesson.shift
    end
    shifts
  end

  def print_shifts(shifts)
    puts '-----------------------------------------------------------'
    shifts.each do |s|
      puts s.day.name+' - '+s.initial_hour.to_s+':'+s.initial_minute.to_s+' a '+s.final_hour.to_s+':'+s.final_minute.to_s
    end
    puts '-----------------------------------------------------------'
  end

  def can_generate_lesson?(period, shift)
    h_i = shift.initial_hour
    m_i = shift.initial_minute
    t_i = h_i >= 12 && h_i < 15 ? add_time(h_i, m_i, 0, 30) : [h_i, m_i]
    h_i = t_i[0]
    m_i = t_i[1]
    h_f = shift.final_hour
    m_f = shift.final_minute
    m = m_f - m_i
    h = h_f - h_i
    if m < 0
      m = m + 60
      h = h - 1
    end
    p_h = period.hours
    p_m = period.minutes
    p_h * 1000 + p_m <= h * 1000 + m
  end

  def add_time(h_1, m_1, h_2, m_2)
    h = h_1 + h_2
    m = m_1 + m_2
    if m >= 60
      m = m - 60
      h = h + 1
    end
    [h, m]
  end

  def generate_lesson(calendar, period, shifts, room)
    commission = period.commission
    subject = commission.subject
    lesson_name = subject.name
    lesson = Lesson.create(calendar: calendar, name: lesson_name, room_id: room.id, period_id: period.id)
    shifts.each do |shift|
      if can_generate_lesson?(period, shift)
        h_i = shift.initial_hour
        m_i = shift.initial_minute
        t_i = h_i >= 12 && h_i < 15 ? add_time(h_i, m_i, 0, 30) : [h_i, m_i]
        h_i = t_i[0]
        m_i = t_i[1]
        time = add_time(h_i, m_i, period.hours, period.minutes)
        h_f = time[0]
        m_f = time[1]
        lesson_shift = Shift.create(initial_hour: h_i, initial_minute: m_i, final_hour: h_f, final_minute: m_f)
        lesson_shift.set_day(shift.day)
        lesson.shift_id = lesson_shift.id
        lesson.save
        return lesson
      end
    end
    lesson.delete
    nil
  end

  def get_subjects(lessons)
    subjects = []
    lessons.each do |lesson|
      subject = lesson.subject
      subjects << subject unless subjects.include? subject
    end
    subjects
  end

  def get_range(lessons)
    h_i = 0
    h_f = 0
    lessons.each do |lesson|
      i = lesson.shift.initial_hour
      f = lesson.shift.final_hour
      h_i = i if h_i == 0 || i < h_i
      h_f = f + 1 if h_f == 0 || f >= h_f
    end
    [h_i, h_f]
  end

  def activate(calendar, activate)
    calendar.active = activate
    lessons = calendar.lessons
    if activate
      Calendar.all.each do |c|
        same = calendar.id != c.id && calendar.career_id == c.career_id && calendar.year == c.year && calendar.quarter == c.quarter && c.active
        activate(c, false) if same
      end
      lessons.each do |lesson|
        shift = lesson.shift
        lesson.period.professors.each do |professor|
          differences = []
          differences(professor.availabilities, [shift]).each do |difference|
            differences << to_av(difference)
          end
          professor.clear_availabilities
          differences.each do |difference|
            professor.availabilities << difference
          end
          professor.save
        end
        room = lesson.room
        differences = []
        differences(room.availabilities, [shift]).each do |difference|
          differences << to_av(difference)
        end
        room.clear_availabilities
        differences.each do |difference|
          room.availabilities << difference
        end
        room.save
      end
    else
      lessons.each do |lesson|
        lesson.period.professors.each do |professor|
          professor.clear_availabilities
          professor.shifts.each do |shift|
            professor.availabilities << to_av(shift)
          end
          professor.save
        end
        room = lesson.room
        room.clear_availabilities
        room.shifts.each do |shift|
          room.availabilities << to_av(shift)
        end
        room.save
      end
    end
    calendar.save
  end

  def to_av(shift)
    availability = Availability.new
    availability.initial_hour = shift.initial_hour
    availability.initial_minute = shift.initial_minute
    availability.final_hour = shift.final_hour
    availability.final_minute = shift.final_minute
    availability.days << shift.day
    availability
  end

  def delete_calendar(calendar)
    calendar.lessons.each do |lesson|
      lesson.shift.destroy unless lesson.shift_id.nil?
      lesson.destroy
    end
    calendar.destroy
  end
end