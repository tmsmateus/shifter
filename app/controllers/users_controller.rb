class UsersController < ApplicationController
  before_action :validate_session

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_url, notice: 'Bienvenido a Shifter'
    else
      render :new
    end
  end

  private
    def user_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end
end
