class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  before_action :validate_session

  # GET /rooms
  # GET /rooms.json
  def index
    @rooms = Room.all
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
    @resources = Resource.all
  end

  # GET /rooms/new
  def new
    @resources = Resource.all
    @room = Room.new
  end

  # GET /rooms/1/edit
  def edit
    @resources = Resource.all
  end

  # POST /rooms
  # POST /rooms.json
  def create
    unless validate_room
      @room = Room.new
      @resources = Resource.all
      @error = true
      respond_to do |format|
        format.html { render :new }
      end
    else
      @room = Room.new(room_params)
      create_room(@room)
      $week.each do |day|
        shift = Shift.create(initial_hour: 7, initial_minute: 0, final_hour: 23, final_minute: 0)
        shift.set_day(day)
        availability = Availability.create(initial_hour: 7, initial_minute: 0, final_hour: 23, final_minute: 0)
        availability.set_day(day)
        @room.add_shift(shift)
        @room.add_availability(availability)
      end
      respond_to do |format|
        if @room.save
          format.html { redirect_to rooms_url, notice: 'El aula se guardó correctamente' }
          format.json { render :show, status: :created, location: @room }
        else
          format.html { render :new }
          format.json { render json: @room.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    unless validate_room
      @resources = Resource.all
      @error = true
      respond_to do |format|
        format.html { render :edit, room_id: @room }
      end
    else
      @room.clear_resources
      create_room(@room)
      respond_to do |format|
        if @room.update(room_params)
          format.html { redirect_to rooms_url, notice: 'El aula se guardó correctamente' }
          format.json { render :show, status: :ok, location: @room }
        else
          format.html { render :edit }
          format.json { render json: @room.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    unless @professor.periods.size == 0
      respond_to do |format|
        format.html { redirect_to professors_url, alert: 'No se pudo eliminar el aula porque está siendo utilizada en algún horario' }
      end
    else
      @room.clear_resources
      @room.clear_availabilities
      @room.destroy
      respond_to do |format|
        format.html { redirect_to rooms_url, notice: 'El aula se eliminó correctamente' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:name)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end

  def create_room(room)
    json = params[:json]
    json.split(',').each do |a|
      resource = Resource.find(a.to_i)
      room.add_resource(resource)
    end
  end

  def validate_room
    !params[:room][:name].empty?
  end
end
