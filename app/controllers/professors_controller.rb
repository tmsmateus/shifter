class ProfessorsController < ApplicationController
  before_action :set_professor, only: [:show, :edit, :update, :destroy, :custom]
  before_action :validate_session

  # GET /professors
  # GET /professors.json
  def index
    @professors = Professor.all
    id = params[:id]
    json = params[:json]
    unless id.nil? || json.nil?
      professor = Professor.find(id)
      unless validate_time(json)
        @professor = professor
        @error = true
        respond_to do |format|
          format.html { render :custom }
        end
      else
        professor_lessons = 0
        professor.periods.each do |p|
          professor_lessons = professor_lessons + p.lessons.size
        end
        professor.clear_shifts
        professor.clear_availabilities if professor_lessons == 0
        json.split(',').each do |a|
          b = a.split(':')
          day = Day.find_by_name(b[0])
          time = b[1]
          shift = Shift.create(initial_hour: time[0,2].to_i, initial_minute: time[2,2].to_i, final_hour: time[4,2].to_i, final_minute: time[6,2].to_i)
          shift.set_day(day)
          professor.add_shift(shift)
          if professor_lessons == 0
            availability = Availability.create(initial_hour: time[0,2].to_i, initial_minute: time[2,2].to_i, final_hour: time[4,2].to_i, final_minute: time[6,2].to_i)
            availability.set_day(day)
            professor.add_availability(availability)
          end
        end
        redirect_to professors_url, notice: 'Los horarios se guardaron correctamente'
      end
    end
  end

  # GET /professors/1
  # GET /professors/1.json
  def show
  end

  # GET /professors/new
  def new
    @professor = Professor.new
  end

  # GET /professors/1/edit
  def edit
  end

  # POST /professors
  # POST /professors.json
  def create
    unless validate_professor(-1)
      @professor = Professor.new
      @error = true
      respond_to do |format|
        format.html { render :new }
      end
    else
      @professor = Professor.new(professor_params)
      respond_to do |format|
        if @professor.save
          format.html { redirect_to professors_url, notice: 'El profesor se guardó correctamente' }
          format.json { render :show, status: :created, location: @professor }
        else
          format.html { render :new }
          format.json { render json: @professor.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /professors/1
  # PATCH/PUT /professors/1.json
  def update
    unless validate_professor(@professor.id)
      @error = true
      respond_to do |format|
        format.html { render :edit, professor_id: @professor }
      end
    else
      respond_to do |format|
        if @professor.update(professor_params)
          format.html { redirect_to professors_url, notice: 'El profesor se guardó correctamente' }
          format.json { render :show, status: :ok, location: @professor }
        else
          format.html { render :edit }
          format.json { render json: @professor.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /professors/1
  # DELETE /professors/1.json
  def destroy
    unless @professor.periods.size == 0
      respond_to do |format|
        format.html { redirect_to professors_url, alert: 'No se pudo eliminar el profesor porque está asignado a materias' }
      end
    else
      @professor.clear_shifts
      @professor.clear_availabilities
      @professor.destroy
      respond_to do |format|
        format.html { redirect_to professors_url, notice: 'El profesor se eliminó correctamente' }
        format.json { head :no_content }
      end
    end
  end

  # GET /professors/1/custom
  def custom
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_professor
      @professor = Professor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def professor_params
      params.require(:professor).permit(:name, :surname, :n)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end

  def validate_professor(id)
    p = params[:professor]
    v_1 = !p[:name].empty?
    v_2 = !p[:surname].empty?
    n = p[:n]
    professor = Professor.find_by_n(n.to_i)
    v_3 = !n.empty? && n.to_i > 0 && (professor.nil? || professor.id == id)
    v_1 && v_2 && v_3
  end

  def validate_time(json)
    json.split(',').each do |a|
      time = a.split(':')[1]
      return false if time[0,4].to_i >= time[4,4].to_i
    end
    true
  end
end
