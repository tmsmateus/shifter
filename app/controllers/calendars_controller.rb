class CalendarsController < ApplicationController
  before_action :set_calendar, only: [:show, :edit, :update, :destroy]
  before_action :validate_session

  # GET /calendars
  # GET /calendars.json
  def index
    @calendars = Calendar.all
  end

  # GET /calendars/1
  # GET /calendars/1.json
  def show
    @new = params[:new]
    @notice = params[:notice]
    @alert = params[:alert]
    @reload = params[:reload]
    @subjects = get_subjects(@calendar.lessons)
    @range = get_range(@calendar.lessons)
  end

  # GET /calendars/new
  def new
    @calendar = Calendar.new
  end

  # GET /calendars/1/edit
  def edit
    redirect_to action: 'show', calendar_id: @calendar.id, reload: true
  end

  # POST /calendars
  # POST /calendars.json
  def create
    @calendar = Calendar.new(calendar_params)

    respond_to do |format|
      if @calendar.save
        format.html { redirect_to @calendar, notice: 'Calendar was successfully created.' }
        format.json { render :show, status: :created, location: @calendar }
      else
        format.html { render :new }
        format.json { render json: @calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /calendars/1
  # PATCH/PUT /calendars/1.json
  def update
    activate(@calendar, !@calendar.active?)
    notice = 'El horario de cursada quedó ' + (@calendar.active? ? 'activo' : 'inactivo')
    respond_to do |format|
      format.html { redirect_to controller: 'calendars', action: 'show', id: @calendar.id, notice: notice }
      format.json { render :show, status: :ok, location: @calendar }
    end
  end

  # DELETE /calendars/1
  # DELETE /calendars/1.json
  def destroy
    if @calendar.active?
      respond_to do |format|
        format.html { redirect_to calendars_url, alert: 'Se debe desactivar el horario para descartarlo' }
      end
    else
      delete_calendar(@calendar)
      respond_to do |format|
        format.html { redirect_to calendars_url, notice: 'Se descartó el horario de cursada' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_calendar
      @calendar = Calendar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def calendar_params
      params.require(:calendar).permit(:n)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end
end
