class PeriodsController < ApplicationController
  before_action :set_period, only: [:show, :edit, :update, :destroy]
  before_action :validate_session

  # GET /periods
  # GET /periods.json
  def index
    @periods = Period.all
  end

  # GET /periods/1
  # GET /periods/1.json
  def show
  end

  # GET /periods/new
  def new
    @professors = Professor.all
    @subjects = Subject.all
  end

  # GET /periods/1/edit
  def edit
  end

  # POST /periods
  # POST /periods.json
  def create
    subject_i = params[:subject_id].to_i
    json = params[:json]
    subject = Subject.find(subject_i)
    periods = subject.periods
    periods.each do |period|
      period.clear_professors
    end
    json.split(',').each do |id|
      professor = Professor.find(id.to_i)
      periods.each do |period|
        professor.add_period(period)
      end
    end

    respond_to do |format|
      format.html { redirect_to new_period_path, notice: 'La materia se asignó a los profesores' }
      format.json { render :show, status: :created, location: @period }
    end
  end

  # PATCH/PUT /periods/1
  # PATCH/PUT /periods/1.json
  def update
    respond_to do |format|
      if @period.update(period_params)
        format.html { redirect_to @period, notice: 'Period was successfully updated.' }
        format.json { render :show, status: :ok, location: @period }
      else
        format.html { render :edit }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /periods/1
  # DELETE /periods/1.json
  def destroy
    @period.destroy
    respond_to do |format|
      format.html { redirect_to periods_url, notice: 'Period was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_period
      @period = Period.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def period_params
      params.require(:period).permit(:n, :hours, :minutes, :commission_id)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end
end
