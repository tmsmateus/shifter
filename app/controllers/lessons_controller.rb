class LessonsController < ApplicationController
  before_action :set_lesson, only: [:show, :edit, :update, :destroy]
  before_action :validate_session

  # GET /lessons
  # GET /lessons.json
  def index
    @calendar = Calendar.find(params[:calendar])
    @open = params[:open]
    @subjects = get_subjects(@calendar.lessons)
    @range = get_range(@calendar.lessons)
  end

  # GET /lessons/1
  # GET /lessons/1.json
  def show
  end

  # GET /lessons/new
  def new
    @careers = Career.all
  end

  # GET /lessons/1/edit
  def edit
  end

  # POST /lessons
  # POST /lessons.json
  def create
    career_id = params[:career_id]
    if career_id
      career = Career.find(career_id.to_i)
      year_i = params[:year].to_i
      quarter_i = params[:quarter].to_i
      subjects = list_subjects(career, year_i, quarter_i)
      if subjects.empty?
        respond_to do |format|
          format.html { redirect_to new_lesson_path, alert: 'El cuatrimestre no contiene materias' }
        end
      else
        data = create_calendar(career, subjects, year_i)
        calendar = data[0]
        pending = data[1]
        i = 0
        while !pending.empty? && i < subjects.size
          delete_calendar(calendar)
          pending_subject = pending[0].subject
          subjects.insert(0, subjects.delete_at(subjects.find_index(pending_subject)))
          data = create_calendar(career, subjects, year_i)
          calendar = data[0]
          pending = data[1]
          i = i + 1
        end
        respond_to do |format|
          if subjects.empty?
            format.html { redirect_to new_lesson_path, alert: 'El cuatrimestre no contiene materias' }
            format.json { render :show, status: :ok, location: @lesson }
          else
            @calendar = calendar
            @subjects = get_subjects(calendar.lessons)
            alert = nil
            pending.each do |p|
              p_name = p.subject.name + ' - Período' + p.n.to_s
              alert = alert.nil? ? 'No se generaron las clases para: ' + p_name : alert + ', ' + p_name
            end
            format.html { redirect_to controller: 'calendars', action: 'show', id: @calendar.id, new: true, notice: 'El horario de cursada se generó correctamente', alert: alert }
          end
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to new_lesson_path, alert: 'Se debe seleccionar una carrera' }
      end
    end
  end

  # PATCH/PUT /lessons/1
  # PATCH/PUT /lessons/1.json
  def update
    respond_to do |format|
      if @lesson.update(lesson_params)
        format.html { redirect_to @lesson, notice: 'Lesson was successfully updated.' }
        format.json { render :show, status: :ok, location: @lesson }
      else
        format.html { render :edit }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lessons/1
  # DELETE /lessons/1.json
  def destroy
    @lesson.destroy
    respond_to do |format|
      format.html { redirect_to lessons_url, notice: 'Lesson was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lesson
      @lesson = Lesson.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lesson_params
      params.require(:lesson).permit(:name, :room_id, :period_id)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end

  def list_subjects(career, year, quarter)
    subjects = []
    c = Career.find(career.id)
    unless c.nil?
      c.subjects.each do |s|
        subjects << s if s.year == year && s.quarter == quarter && !subjects.include?(s)
      end
    end
    subjects
  end

  def shuffle_subjects(subjects)
    subjects.insert(0, subjects.delete_at(subjects.size - 1))
    subjects
  end

  def p(subjects)
    x = []
    subjects.each do |l|
      x << l.name unless x.include? l.name
    end
    a = ""
    x.each do |w|
      a = a + w + ", "
    end
    a
  end

  def create_calendar(career, subjects, year_i)
    lessons = []
    n = 1
    while !Calendar.find_by_n(n).nil?
      n = n + 1
    end
    calendar = Calendar.create(n: n, active: false)
    rooms = Room.all
    pending = Array.new
    subjects.each do |subject|
      # tener en cuenta turnos de año
      year = Year.where(career_id: career.id, n: year_i)[0]
      subject.commissions.each do |commission|
        commission.periods.each do |period|
          i = 0
          while i < rooms.size
            room = rooms[i]
            has_resources = true
            period.resources.each do |resource|
              has_resources = false unless resource.rooms.include?(room)
            end
            if has_resources
              # tener en cuenta turnos del aula
              shifts = intersections(year.shifts, room.availabilities)
              lessons_shifts = get_shifts(lessons)
              # tener en cuenta turnos de clases creadas
              period_shifts = differences(shifts, lessons_shifts)
              # tener en cuenta turnos de profesores
              period.professors.each do |professor|
                period_shifts = intersections(period_shifts, professor.availabilities) unless professor.availabilities.empty?
              end
              lesson = generate_lesson(calendar, period, period_shifts, room)
              if lesson.nil?
                pending << period unless pending.include? period
                i = i + 1
              else
                lessons << lesson unless lesson.nil?
                pending.delete period if pending.include? period
                i = rooms.size
              end
            else
              i = i + 1
            end
          end
        end
      end
    end
    [calendar, pending]
  end
end