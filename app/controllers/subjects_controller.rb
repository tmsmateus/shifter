class SubjectsController < ApplicationController
  before_action :set_subject, only: [:show, :edit, :update, :destroy, :custom]
  before_action :validate_session

  # GET /subjects
  # GET /subjects.json
  def index
    @subjects = Subject.all
    id = params[:id]
    json = params[:json]
    unless id.nil? || json.nil?
      subject = Subject.find(id)
      subject.periods.each do |period|
        period.clear_resources
      end
      json.split(';').each do |a|
        b = a.split(':')
        period = Period.where(commission_id: subject.commissions[0].id, n: b[0])[0]
        period.clear_resources
        b[1].split(',').each do |r_id|
          resource = Resource.find(r_id)
          period.add_resource(resource)
        end
      end
      redirect_to subjects_url, notice: 'Los recursos se asignaron correctamente'
    end
  end

  # GET /subjects/1
  # GET /subjects/1.json
  def show
  end

  # GET /subjects/new
  def new
    @careers = Career.all
    @subject = Subject.new
    @new = true
  end

  # GET /subjects/1/edit
  def edit
    @careers = Career.all
  end

  # POST /subjects
  # POST /subjects.json
  def create
    unless validate_subject
      @subject = Subject.new
      @careers = Career.all
      @new = true
      @error = true
      respond_to do |format|
        format.html { render :new }
      end
    else
      @subject = Subject.new(subject_params)
      create_subject(@subject)
      save = @subject.save
      career_id = params[:career_id]
      career = Career.find(career_id)
      career.add_subject(@subject)
      respond_to do |format|
        if save
          format.html { redirect_to subjects_url, notice: 'La materia se guardó correctamente' }
          format.json { render :show, status: :created, location: @subject }
        else
          format.html { render :new }
          format.json { render json: @subject.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /subjects/1
  # PATCH/PUT /subjects/1.json
  def update
    unless validate_subject && can_clear
      @careers = Career.all
      @error = true
      respond_to do |format|
        format.html { render :edit, subject_id: @subject }
      end
    else
      clear_subject(@subject)
      create_subject(@subject)
      save = @subject.update(subject_params)
      career_id = params[:career_id]
      career = Career.find(career_id)
      career.add_subject(@subject)
      respond_to do |format|
        if save
          format.html { redirect_to subjects_url, notice: 'La materia se guardó correctamente' }
          format.json { render :show, status: :ok, location: @subject }
        else
          format.html { render :edit }
          format.json { render json: @subject.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /subjects/1
  # DELETE /subjects/1.json
  def destroy
    unless can_clear
      respond_to do |format|
        format.html { redirect_to subjects_url, alert: 'No se pudo eliminar la materia porque está asociada a otras tablas' }
      end
    else
      clear_subject(@subject)
      @subject.destroy
      respond_to do |format|
        format.html { redirect_to subjects_url, notice: 'La materia se eliminó correctamente' }
        format.json { head :no_content }
      end
    end
  end

  def custom
    @resources = Resource.all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      @subject = Subject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_params
      params.require(:subject).permit(:name, :year, :quarter)
    end

    def validate_session
      redirect_to new_session_path unless current_user
    end

  def create_subject(subject)
    periods = params[:periods]
    commission = Commission.create(subject: subject, n: 1)
    i = 0
    hours = 0
    minutes = 0
    while i < periods.to_i do
      i = i + 1
      h_1 = params[:hours_1]
      m_1 = params[:minutes_1]
      h_2 = params[:hours_2]
      m_2 = params[:minutes_2]
      h_3 = params[:hours_3]
      m_3 = params[:minutes_3]
      h_4 = params[:hours_4]
      m_4 = params[:minutes_4]
      h = (i==1 ? h_1 : i==2 ? h_2 : i==3 ? h_3 : h_4).to_i
      m = (i==1 ? m_1 : i==2 ? m_2 : i==3 ? m_3 : m_4).to_i
      Period.create(commission: commission, n: i, hours: h, minutes: m)
      hours = hours + h
      minutes = minutes + m
    end
    while minutes >= 60
      minutes = minutes - 60
      hours = hours + 1
    end
    subject.hours = hours
    subject.minutes = minutes
  end

  def clear_subject(subject)
    subject.clear_periods
    subject.clear_commissions
  end

  def validate_subject
    v_1 = !params[:subject][:name].empty?
    year = params[:subject][:year]
    quarter = params[:subject][:quarter]
    career = Career.find(params[:career_id])
    v_2 = !year.empty? && year.to_i > 0 && year.to_i <= career.years.size
    v_3 = !quarter.empty? && quarter.to_i > 0 && quarter.to_i <= $quarters
    h_1 = params[:hours_1]
    h_2 = params[:hours_2]
    h_3 = params[:hours_3]
    h_4 = params[:hours_4]
    periods = params[:periods].to_i
    v_4 = !h_1.empty? && h_1.to_i > 0 && h_1.to_i < 10
    v_5 = periods < 2 || (!h_2.empty? && h_2.to_i > 0 && h_2.to_i < 10)
    v_6 = periods < 3 || (!h_3.empty? && h_3.to_i > 0 && h_3.to_i < 10)
    v_7 = periods < 4 || (!h_4.empty? && h_4.to_i > 0 && h_4.to_i < 10)
    v_1 && v_2 && v_3 && v_4 && v_5 && v_6 && v_7
  end

  def can_clear
    @subject.periods.each do |period|
      return false if period.professors.size > 0 || Lesson.where(period_id: period.id).size > 0
    end
    true
  end
end
