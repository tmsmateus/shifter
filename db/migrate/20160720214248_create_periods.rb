class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periods do |t|
      t.integer :n
      t.integer :hours
      t.integer :minutes
      t.references :commission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
