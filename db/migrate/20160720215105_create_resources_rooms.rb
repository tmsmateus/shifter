class CreateResourcesRooms < ActiveRecord::Migration
  def change
    create_table :resources_rooms do |t|
      t.references :resource, index: true, foreign_key: true
      t.references :room, index: true, foreign_key: true
    end
  end
end
