class CreateCareersSubjects < ActiveRecord::Migration
  def change
    create_table :careers_subjects, index: false do |t|
      t.references :career, index: true, foreign_key: true
      t.references :subject, index: true, foreign_key: true
    end
  end
end
