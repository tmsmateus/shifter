class CreateShiftsYears < ActiveRecord::Migration
  def change
    create_table :shifts_years, index: false do |t|
      t.references :shift, index: true, foreign_key: true
      t.references :year, index: true, foreign_key: true
    end
  end
end
