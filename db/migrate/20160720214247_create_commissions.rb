class CreateCommissions < ActiveRecord::Migration
  def change
    create_table :commissions do |t|
      t.integer :n
      t.references :subject, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
