class CreatePeriodsResources < ActiveRecord::Migration
  def change
    create_table :periods_resources do |t|
      t.references :period, index: true, foreign_key: true
      t.references :resource, index: true, foreign_key: true
    end
  end
end
