class CreateAvailabilitiesProfessors < ActiveRecord::Migration
  def change
    create_table :availabilities_professors, index: false do |t|
      t.references :availability, index: true, foreign_key: true
      t.references :professor, index: true, foreign_key: true
    end
  end
end
