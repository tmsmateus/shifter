class CreatePeriodsProfessors < ActiveRecord::Migration
  def change
    create_table :periods_professors do |t|
      t.references :period, index: true, foreign_key: true
      t.references :professor, index: true, foreign_key: true
    end
  end
end
