class CreateRoomsShifts < ActiveRecord::Migration
  def change
    create_table :rooms_shifts, index: false do |t|
      t.references :room, index: true, foreign_key: true
      t.references :shift, index: true, foreign_key: true
    end
  end
end
