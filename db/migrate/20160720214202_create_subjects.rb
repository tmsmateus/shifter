class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :name
      t.integer :hours
      t.integer :minutes
      t.integer :year
      t.integer :quarter

      t.timestamps null: false
    end
  end
end
