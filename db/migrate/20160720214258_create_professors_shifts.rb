class CreateProfessorsShifts < ActiveRecord::Migration
  def change
    create_table :professors_shifts, index: false do |t|
      t.references :professor, index: true, foreign_key: true
      t.references :shift, index: true, foreign_key: true
    end
  end
end
