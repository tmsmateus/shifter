class CreateAvailabilities < ActiveRecord::Migration
  def change
    create_table :availabilities do |t|
      t.integer :initial_hour
      t.integer :initial_minute
      t.integer :final_hour
      t.integer :final_minute

      t.timestamps null: false
    end
  end
end
