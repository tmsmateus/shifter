class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.string :name
      t.references :calendar, index: true, foreign_key: true
      t.references :room, index: true, foreign_key: true
      t.references :period, index: true, foreign_key: true
      t.references :shift, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
