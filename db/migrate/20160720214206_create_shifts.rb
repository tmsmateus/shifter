class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.integer :initial_hour
      t.integer :initial_minute
      t.integer :final_hour
      t.integer :final_minute

      t.timestamps null: false
    end
  end
end
