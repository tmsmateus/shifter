class CreateLessonsShifts < ActiveRecord::Migration
  def change
    create_table :lessons_shifts, index: false do |t|
      t.references :lesson, index: true, foreign_key: true
      t.references :shift, index: true, foreign_key: true
    end
  end
end
