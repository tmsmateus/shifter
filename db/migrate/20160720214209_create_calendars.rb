class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.integer :n
      t.boolean :active

      t.timestamps null: false
    end
  end
end
