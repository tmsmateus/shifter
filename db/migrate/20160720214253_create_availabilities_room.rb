class CreateAvailabilitiesRoom < ActiveRecord::Migration
  def change
    create_table :availabilities_rooms, index: false do |t|
      t.references :availability, index: true, foreign_key: true
      t.references :room, index: true, foreign_key: true
    end
  end
end
