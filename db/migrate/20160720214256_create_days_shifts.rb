class CreateDaysShifts < ActiveRecord::Migration
  def change
    create_table :days_shifts, index: false do |t|
      t.references :day, index: true, foreign_key: true
      t.references :shift, index: true, foreign_key: true
    end
  end
end
