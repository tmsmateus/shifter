class CreateAvailabilitiesDays < ActiveRecord::Migration
  def change
    create_table :availabilities_days, index: false do |t|
      t.references :availability, index: true, foreign_key: true
      t.references :day, index: true, foreign_key: true
    end
  end
end
