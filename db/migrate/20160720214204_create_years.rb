class CreateYears < ActiveRecord::Migration
  def change
    create_table :years do |t|
      t.references :career, index: true, foreign_key: true
      t.references :subject, index: true, foreign_key: true
      t.integer :n

      t.timestamps null: false
    end
  end
end
