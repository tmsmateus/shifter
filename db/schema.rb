# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161108235422) do

  create_table "availabilities", force: :cascade do |t|
    t.integer  "initial_hour"
    t.integer  "initial_minute"
    t.integer  "final_hour"
    t.integer  "final_minute"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "availabilities_days", force: :cascade do |t|
    t.integer "availability_id"
    t.integer "day_id"
  end

  add_index "availabilities_days", ["availability_id"], name: "index_availabilities_days_on_availability_id"
  add_index "availabilities_days", ["day_id"], name: "index_availabilities_days_on_day_id"

  create_table "availabilities_professors", force: :cascade do |t|
    t.integer "availability_id"
    t.integer "professor_id"
  end

  add_index "availabilities_professors", ["availability_id"], name: "index_availabilities_professors_on_availability_id"
  add_index "availabilities_professors", ["professor_id"], name: "index_availabilities_professors_on_professor_id"

  create_table "availabilities_rooms", force: :cascade do |t|
    t.integer "availability_id"
    t.integer "room_id"
  end

  add_index "availabilities_rooms", ["availability_id"], name: "index_availabilities_rooms_on_availability_id"
  add_index "availabilities_rooms", ["room_id"], name: "index_availabilities_rooms_on_room_id"

  create_table "calendars", force: :cascade do |t|
    t.integer  "n"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "careers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "careers_subjects", force: :cascade do |t|
    t.integer "career_id"
    t.integer "subject_id"
  end

  add_index "careers_subjects", ["career_id"], name: "index_careers_subjects_on_career_id"
  add_index "careers_subjects", ["subject_id"], name: "index_careers_subjects_on_subject_id"

  create_table "commissions", force: :cascade do |t|
    t.integer  "n"
    t.integer  "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "commissions", ["subject_id"], name: "index_commissions_on_subject_id"

  create_table "days", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "days_shifts", force: :cascade do |t|
    t.integer "day_id"
    t.integer "shift_id"
  end

  add_index "days_shifts", ["day_id"], name: "index_days_shifts_on_day_id"
  add_index "days_shifts", ["shift_id"], name: "index_days_shifts_on_shift_id"

  create_table "lessons", force: :cascade do |t|
    t.string   "name"
    t.integer  "calendar_id"
    t.integer  "room_id"
    t.integer  "period_id"
    t.integer  "shift_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "lessons", ["calendar_id"], name: "index_lessons_on_calendar_id"
  add_index "lessons", ["period_id"], name: "index_lessons_on_period_id"
  add_index "lessons", ["room_id"], name: "index_lessons_on_room_id"
  add_index "lessons", ["shift_id"], name: "index_lessons_on_shift_id"

  create_table "lessons_shifts", force: :cascade do |t|
    t.integer "lesson_id"
    t.integer "shift_id"
  end

  add_index "lessons_shifts", ["lesson_id"], name: "index_lessons_shifts_on_lesson_id"
  add_index "lessons_shifts", ["shift_id"], name: "index_lessons_shifts_on_shift_id"

  create_table "periods", force: :cascade do |t|
    t.integer  "n"
    t.integer  "hours"
    t.integer  "minutes"
    t.integer  "commission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "periods", ["commission_id"], name: "index_periods_on_commission_id"

  create_table "periods_professors", force: :cascade do |t|
    t.integer "period_id"
    t.integer "professor_id"
  end

  add_index "periods_professors", ["period_id"], name: "index_periods_professors_on_period_id"
  add_index "periods_professors", ["professor_id"], name: "index_periods_professors_on_professor_id"

  create_table "periods_resources", force: :cascade do |t|
    t.integer "period_id"
    t.integer "resource_id"
  end

  add_index "periods_resources", ["period_id"], name: "index_periods_resources_on_period_id"
  add_index "periods_resources", ["resource_id"], name: "index_periods_resources_on_resource_id"

  create_table "professors", force: :cascade do |t|
    t.string   "name"
    t.string   "surname"
    t.integer  "n"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "professors_shifts", force: :cascade do |t|
    t.integer "professor_id"
    t.integer "shift_id"
  end

  add_index "professors_shifts", ["professor_id"], name: "index_professors_shifts_on_professor_id"
  add_index "professors_shifts", ["shift_id"], name: "index_professors_shifts_on_shift_id"

  create_table "resources", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "resources_rooms", force: :cascade do |t|
    t.integer "resource_id"
    t.integer "room_id"
  end

  add_index "resources_rooms", ["resource_id"], name: "index_resources_rooms_on_resource_id"
  add_index "resources_rooms", ["room_id"], name: "index_resources_rooms_on_room_id"

  create_table "rooms", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rooms_shifts", force: :cascade do |t|
    t.integer "room_id"
    t.integer "shift_id"
  end

  add_index "rooms_shifts", ["room_id"], name: "index_rooms_shifts_on_room_id"
  add_index "rooms_shifts", ["shift_id"], name: "index_rooms_shifts_on_shift_id"

  create_table "shifts", force: :cascade do |t|
    t.integer  "initial_hour"
    t.integer  "initial_minute"
    t.integer  "final_hour"
    t.integer  "final_minute"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "shifts_years", force: :cascade do |t|
    t.integer "shift_id"
    t.integer "year_id"
  end

  add_index "shifts_years", ["shift_id"], name: "index_shifts_years_on_shift_id"
  add_index "shifts_years", ["year_id"], name: "index_shifts_years_on_year_id"

  create_table "subjects", force: :cascade do |t|
    t.string   "name"
    t.integer  "hours"
    t.integer  "minutes"
    t.integer  "year"
    t.integer  "quarter"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "years", force: :cascade do |t|
    t.integer  "career_id"
    t.integer  "subject_id"
    t.integer  "n"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "years", ["career_id"], name: "index_years_on_career_id"
  add_index "years", ["subject_id"], name: "index_years_on_subject_id"

end
